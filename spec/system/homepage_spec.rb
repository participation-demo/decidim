# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Homepage", type: :system do
  it "redirects to system" do
    visit "/"

    expect(page).to have_content("You must create an organization to get started.")
  end
end
