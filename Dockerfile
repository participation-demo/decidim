FROM ruby:2.5.1-alpine3.7
LABEL maintainer="deivid.rodriguez@riseup.net"

WORKDIR /app

ENV GEM_HOME /usr/local/bundle
ENV BUNDLE_PATH /app/.bundle
ENV PATH $GEM_HOME/bin:$BUNDLE_PATH/gems/bin:$PATH

RUN apk add --no-cache nodejs postgresql-dev imagemagick tzdata

RUN apk add --no-cache --virtual build-dependencies make g++
COPY Gemfile Gemfile.lock ./
RUN bundle install
RUN apk del build-dependencies

COPY . ./

RUN SECRET_KEY_BASE=rails_issue_32947 bin/rails assets:precompile

COPY bin/docker-entrypoint.sh /usr/local/bin

ENTRYPOINT ["docker-entrypoint.sh"]
